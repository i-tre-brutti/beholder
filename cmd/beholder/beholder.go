package main

import (
	"errors"
	"flag"
	"log"
	"sync"

	"gitlab.com/i-tre-brutti/beholder/analyzers"
	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"

	"gopkg.in/mgo.v2/bson"
)

type ProjectAnalysis struct {
	status interfaces.ProjectStatus
}

func main() {
	// Manage database connection
	db.InitSession()
	defer db.CloseSession()

	var waitGroup sync.WaitGroup
	confs, err := getSelectedConf()
	utils.E(err, 0)
	for _, selectedConfs := range confs {
		log.Println("Spawning analyzer for project", selectedConfs.ProjectID)
		waitGroup.Add(1)
		go func(c interfaces.ProjectConf) {
			defer waitGroup.Done()
			var project ProjectAnalysis
			project.status = analyzers.StartAnalysis(c)
			project.SaveToDB()
		}(selectedConfs)
	}

	waitGroup.Wait()
}

// SaveToDB the result of the analysis
func (p *ProjectAnalysis) SaveToDB() {
	log.Println("Saving project status for", p.status.ProjectID)
	collection := db.GetStatusCollection()
	var currentStatus interfaces.ProjectStatus

	// Find the current status of the project in the db, if it exists
	err := collection.Find(bson.M{"project_id": p.status.ProjectID, "document_version": p.status.DocumentVersion}).One(&currentStatus)
	if err != nil {
		// The status doesn't exist, create it
		// err is of type mgo.ErrNotFound, e.g. `errors.New("not found")` so we need to check
		// its content as a string
		if err.Error() == "not found" {
			log.Println("Creating new status for project", p.status.ProjectID)
			insertError := collection.Insert(p.status)
			utils.E(insertError, 0)
			return
		}
		utils.E(err, 0)
	}

	// The status exists, update it
	log.Println("Updating status for project", p.status.ProjectID)
	updateErr := collection.Update(bson.M{"project_id": currentStatus.ProjectID, "document_version": p.status.DocumentVersion}, p.status)
	utils.E(updateErr, 0)
}

func getProjectsConfs() ([]interfaces.ProjectConf, error) {
	var result []interfaces.ProjectConf

	err := db.GetConfCollection().Find(bson.M{}).All(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func getSelectedConf() ([]interfaces.ProjectConf, error) {
	projectID := flag.String("P", "", "Project ID")
	flag.Parse()

	confs, err := getProjectsConfs()
	if err != nil {
		return nil, err
	}

	var selectedConfs []interfaces.ProjectConf
	if *projectID != "" {
		for _, c := range confs {
			if c.ProjectID == *projectID {
				selectedConfs = append(selectedConfs, c)
				log.Println("Initializing project", c.ProjectID)
				return selectedConfs, nil
			}
		}
		if len(selectedConfs) == 0 {
			return nil, errors.New("cannot find the required project")
		}
	}
	log.Println("Analyzing all projects")
	return confs, nil
}
