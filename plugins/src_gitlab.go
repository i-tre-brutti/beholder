package plugins

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"sync"
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

// Init initialize the plugin setting the configuration and the previous status
func (v *SrcGitlabPlugin) Init(conf interfaces.SrcRepoConf, status interfaces.SrcData) {
	v.conf = conf
	v.status = status
	v.baseURL = conf.BaseURL
	if v.conf.BaseURL == "" {
		v.baseURL = "https://gitlab.com"
	}
	v.authHeader = utils.AuthHeader{Name: "PRIVATE-TOKEN", Value: conf.AuthToken}
}

// Fetch gets info from APIs and populates the new status
func (v *SrcGitlabPlugin) Fetch() (interfaces.SrcData, string) {
	var lastCommitID string
	project := url.PathEscape(v.conf.ProjectID)
	var newStatus interfaces.SrcData
	// TODO: use the old status v.status as base for the new one
	// newStatus = v.status
	var waitGroup sync.WaitGroup
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Code, lastCommitID = v.fetchCodeInfo(project)
	}()
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Branches = v.fetchBranchesInfo(project)
	}()
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Pr = v.fetchMergeRequestsInfo(project)
	}()
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Social = v.fetchSocialInfo(project)
	}()
	waitGroup.Wait()

	return newStatus, lastCommitID
}

func (v *SrcGitlabPlugin) fetchSocialInfo(project string) interfaces.SrcSocial {
	log.Println("Fetching social info about project", project)
	var result interfaces.SrcSocial
	var waitGroup sync.WaitGroup

	var stars int
	var forks int
	var contributors int

	// Stars and Forks
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		var p projectInfo
		err := utils.GetJSON(fmt.Sprintf("%s/api/v4/projects/%s", v.baseURL, project), v.authHeader, &p)
		if err != nil {
			log.Fatalln("Error getting social info")
		}
		stars = p.StarCount
		forks = p.ForksCount
	}()

	// Contributors
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		var c []contributor
		err := utils.GetJSON(fmt.Sprintf("%s/api/v4/projects/%s/repository/contributors", v.baseURL, project), v.authHeader, &c)
		if err != nil {
			log.Fatalln("Error getting contributors")
		}
		contributors = len(c)
	}()

	waitGroup.Wait()

	result.Contributors = contributors
	result.Forks = forks
	result.Stars = stars
	// Watchers isn't applicable on gitlab
	result.Watchers = 0

	return result
}

func (v *SrcGitlabPlugin) fetchMergeRequestsInfo(project string) interfaces.SrcPr {
	var result interfaces.SrcPr
	var mrs []mergeRequest
	err := utils.GetJSON(fmt.Sprintf("%s/api/v4/projects/%s/merge_requests", v.baseURL, project), v.authHeader, &mrs)
	if err != nil {
		log.Fatalln("Error getting merge requests")
	}

	for _, mr := range mrs {
		log.Printf("Analyzing merge-request %s (%d)", mr.Title, mr.ID)
		result.Total++
		switch mr.State {
		case "opened":
			result.Opened++
		case "closed":
			result.Closed++
			if mr.DownVotes > mr.UpVotes {
				result.Rejected++
			}
		case "merged":
			result.Merged++
			if mr.UpVotes > mr.DownVotes {
				result.Approved++
			}
		}
	}
	return result
}

func (v *SrcGitlabPlugin) fetchCodeInfo(project string) (interfaces.SrcCode, string) {
	log.Println("Fetching code info about project", project)
	var result interfaces.SrcCode
	var waitGroup sync.WaitGroup

	var lastCommitID string

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		lastCommitID = v.fetchCommits(project, &result)
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		v.fetchCodeSize(project, &result)
	}()

	waitGroup.Wait()

	return result, lastCommitID
}

func (v *SrcGitlabPlugin) fetchBranchesInfo(project string) interfaces.SrcBranch {
	log.Println("Fetching branches info about project", project)
	var result interfaces.SrcBranch
	var branches []branch
	err := utils.GetJSON(fmt.Sprintf("%s/api/v4/projects/%s/repository/branches", v.baseURL, project), v.authHeader, &branches)
	utils.E(err, 0)

	for _, b := range branches {
		log.Println("Analyzing branch", b.Name)
		if b.Merged {
			result.Merged++
		} else {
			// Open branches with the last commit older than 1 month is considered "abandoned"
			oldDate := time.Now().AddDate(0, -1, 0)
			commitDate, err := time.Parse(gitlabDateFormat, b.Commit.CommittedDate)
			if err != nil {
				log.Println("Date parse error", b.Commit.CommittedDate)
				panic(err)
			}
			if commitDate.Before(oldDate) {
				result.OpenAbandoned++
			} else {
				result.OpenWip++
			}
		}
	}

	return result
}

/*
 * Get the file path and size from GitLab API
 */
func (v *SrcGitlabPlugin) getFileSize(project, filePath string) interfaces.RepoFileSize {
	var file repoFileDetails
	escapedFilePath := url.PathEscape(filePath)
	url := fmt.Sprintf("%s/api/v4/projects/%s/repository/files/%s?ref=master", v.baseURL, project, escapedFilePath)
	log.Println("Getting file info for:", url)
	err := utils.GetJSON(url, v.authHeader, &file)
	if err != nil {
		log.Fatalln("Error getting file size")
	}
	return interfaces.RepoFileSize{Path: filePath, Size: file.Size}
}

func (v *SrcGitlabPlugin) fetchCodeSize(project string, result *interfaces.SrcCode) {
	log.Println("Fetching code size")

	var files []repoFile

	url := fmt.Sprintf("%s/api/v4/projects/%s/repository/tree?recursive=true", v.baseURL, project)
	res, err := makeAPICall(url, v.authHeader)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(res, &files)
	if err != nil {
		log.Println(err)
	}

	var sizePerFile []interfaces.RepoFileSize
	var totalSize int
	if false {
		// This is super-fast, but we get "429: Too many requests" :(
		sizePerFile, totalSize = parallelCodeSizeFetcher(v.getFileSize, project, files)
	} else {
		// Slow version
		sizePerFile, totalSize = serialCodeSizeFetcher(v.getFileSize, project, files)
	}

	result.CodeSizePerFile = sizePerFile
	result.TotalCodeSize = totalSize
}

func (v *SrcGitlabPlugin) fetchCommits(project string, result *interfaces.SrcCode) string {
	log.Println("Fetching commits")
	var lastCommitID string
	var commits []commit

	url := fmt.Sprintf("%s/api/v4/projects/%s/repository/commits", v.baseURL, project)
	res, err := makeAPICall(url, v.authHeader)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(res, &commits)
	if err != nil {
		log.Println(err)
	}

	if len(commits) > 0 {
		lastCommitID = commits[0].ID
	}
	result.CommitsPerMonth = calculateCommitsPerMonth(commits)

	result.FirstCommit, err = time.Parse(gitlabDateFormat, commits[len(commits)-1].CreatedAt)
	if err != nil {
		panic(err)
	}

	result.LastCommit, err = time.Parse(gitlabDateFormat, commits[0].CreatedAt)
	if err != nil {
		panic(err)
	}

	return lastCommitID
}
