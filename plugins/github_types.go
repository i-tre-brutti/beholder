package plugins

import (
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

type SrcGithubPlugin struct {
	conf       interfaces.SrcRepoConf
	status     interfaces.SrcData
	baseURL    string
	authHeader utils.AuthHeader
}

type IssueGithubPlugin struct {
	conf       interfaces.IssueRepoConf
	status     interfaces.IssueData
	baseURL    string
	authHeader utils.AuthHeader
}

type githubProjectInfo struct {
	StarCount     int `json:"stargazers_count"`
	ForksCount    int `json:"forks_count"`
	WatchersCount int `json:"watchers_count"`
}

type githubContributor struct {
	Login         string `json:"login"`
	ID            int    `json:"id"`
	Contributions int    `json:"contributions"`
}

type githubPullRequest struct {
	Number   int    `json:"number"`
	Title    string `json:"title"`
	State    string `json:"state"`
	ClosedAt string `json:"closed_at"`
	MergedAt string `json:"merged_at"`
}

type githubPullRequestReview struct {
	ID    int    `json:"id"`
	State string `json:"state"`
}

type githubCommit struct {
	SHA    string `json:"sha"`
	Commit struct {
		Committer githubUserCompact `json:"committer"`
	} `json:"commit"`
}

type githubUserCompact struct {
	Name  string    `json:"name"`
	Email string    `json:"email"`
	Date  time.Time `json:"date"`
}

type githubContent struct {
	Name string `json:"name"`
	Path string `json:"path"`
	Sha  string `json:"sha"`
	Size int    `json:"size"`
	Type string `json:"type"`
}

type githubIssue struct {
	Number    int       `json:"number"`
	Title     string    `json:"title"`
	State     string    `json:"state"`
	CreatedAt time.Time `json:"created_at"`
	ClosedAt  time.Time `json:"closed_at"`
}
