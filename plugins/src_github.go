package plugins

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

// Init initialize the plugin setting the configuration and the previous status
func (v *SrcGithubPlugin) Init(conf interfaces.SrcRepoConf, status interfaces.SrcData) {
	v.conf = conf
	v.status = status
	v.baseURL = "https://api.github.com"
	v.authHeader = utils.AuthHeader{Name: "Authorization", Value: fmt.Sprintf("token %v", conf.AuthToken)}
}

// Fetch asdsad
func (v *SrcGithubPlugin) Fetch() (interfaces.SrcData, string) {
	var lastCommitID string
	var newStatus interfaces.SrcData
	// TODO: use the old status v.status as base for the new one
	// newStatus = v.status
	var waitGroup sync.WaitGroup

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Code, lastCommitID = v.fetchCodeInfo(v.conf.ProjectID)
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Branches = v.fetchBranchesInfo(v.conf.ProjectID)
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Pr = v.fetchPullRequestsInfo(v.conf.ProjectID)
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus.Social = v.fetchSocialInfo(v.conf.ProjectID)
	}()

	waitGroup.Wait()
	return newStatus, lastCommitID
}

func (v *SrcGithubPlugin) fetchCodeInfo(project string) (interfaces.SrcCode, string) {
	log.Println("Fetching code info about project", project)
	var result interfaces.SrcCode
	var waitGroup sync.WaitGroup

	var lastCommitID string

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		lastCommitID = v.fetchCommits(project, &result)
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		v.fetchCodeSize(project, &result)
	}()

	waitGroup.Wait()

	return result, lastCommitID
}

func (v *SrcGithubPlugin) fetchPullRequestsInfo(project string) interfaces.SrcPr {
	var result interfaces.SrcPr
	var prs []githubPullRequest
	err := utils.GetJSON(fmt.Sprintf("%s/repos/%s/pulls?state=all", v.baseURL, project), v.authHeader, &prs)
	utils.E(err, 0)
	for _, pr := range prs {
		log.Printf("Analyzing pull-request %s (%d)", pr.Title, pr.Number)
		result.Total++
		switch pr.State {
		case "open":
			result.Opened++
		case "closed":
			if pr.MergedAt != "" {
				result.Merged++
			} else if pr.MergedAt == "" && pr.ClosedAt != "" {
				result.Closed++
			}
		}
		// Check approval status
		var reviews []githubPullRequestReview
		var approved int
		var rejected int

		err := utils.GetJSON(fmt.Sprintf("%s/repos/%s/pulls/%d/reviews", v.baseURL, project, pr.Number), v.authHeader, &reviews)
		utils.E(err, 0)

		for _, r := range reviews {
			if r.State == "APPROVED" {
				approved++
			} else if r.State == "CHANGES_REQUESTED" {
				rejected++
			}
		}
		if approved > 0 && approved >= rejected {
			result.Approved++
		} else if rejected > 0 && rejected > approved {
			result.Rejected++
		}
	}
	return result
}

func (v *SrcGithubPlugin) fetchBranchesInfo(project string) interfaces.SrcBranch {
	var result interfaces.SrcBranch
	return result
}

func (v *SrcGithubPlugin) fetchSocialInfo(project string) interfaces.SrcSocial {
	var result interfaces.SrcSocial
	var waitGroup sync.WaitGroup

	var stars int
	var forks int
	var contributors int
	var watchers int

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		var p githubProjectInfo
		err := utils.GetJSON(fmt.Sprintf("%s/repos/%s", v.baseURL, project), v.authHeader, &p)
		utils.E(err, 0)

		stars = p.StarCount
		forks = p.ForksCount
		watchers = p.WatchersCount
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		var c []githubContributor
		err := utils.GetJSON(fmt.Sprintf("%s/repos/%s/contributors", v.baseURL, project), v.authHeader, &c)
		utils.E(err, 0)

		contributors = len(c)
	}()

	waitGroup.Wait()

	result.Contributors = contributors
	result.Forks = forks
	result.Stars = stars
	result.Watchers = watchers

	return result
}

func (v *SrcGithubPlugin) fetchCommits(project string, result *interfaces.SrcCode) string {
	log.Println("Fetching commits")
	var lastCommitID string
	var commits []githubCommit

	url := fmt.Sprintf("%s/repos/%s/commits", v.baseURL, project)
	res, err := makeGithubAPICall(url, v.authHeader)
	utils.E(err, 0)

	err = json.Unmarshal(res, &commits)
	utils.E(err, 2)

	if len(commits) > 0 {
		lastCommitID = commits[0].SHA
	}

	result.CommitsPerMonth = v.calculateCommitsPerMonth(commits)

	result.FirstCommit = commits[len(commits)-1].Commit.Committer.Date
	result.LastCommit = commits[0].Commit.Committer.Date

	return lastCommitID
}

func (v *SrcGithubPlugin) calculateCommitsPerMonth(commits []githubCommit) map[string]int {
	commitsPerMonth := make(map[string]int)
	for _, c := range commits {
		t := c.Commit.Committer.Date
		dateStamp := fmt.Sprintf("%04d%02d", t.Year(), t.Month())
		commitsPerMonth[dateStamp]++
	}

	return commitsPerMonth
}

func (v *SrcGithubPlugin) fetchCodeSize(project string, result *interfaces.SrcCode) {
	log.Println("Fetching code size")

	totalSize, sizePerFile := v.fetchDirFiles(fmt.Sprintf("%s/repos/%s/contents", v.baseURL, project), "")

	result.CodeSizePerFile = sizePerFile
	result.TotalCodeSize = totalSize
}

func (v *SrcGithubPlugin) fetchDirFiles(contentURL, path string) (int, []interfaces.RepoFileSize) {
	var sizePerFile []interfaces.RepoFileSize
	var files []githubContent

	finalURL := fmt.Sprintf("%s/%s", contentURL, path)
	res, err := makeGithubAPICall(finalURL, v.authHeader)
	utils.E(err, 0)

	err = json.Unmarshal(res, &files)
	utils.E(err, 0)

	totalSize := 0
	for _, f := range files {
		if f.Type == "dir" {
			t, sizePerFileInDir := v.fetchDirFiles(contentURL, f.Path)
			totalSize += t
			sizePerFile = append(sizePerFile, sizePerFileInDir...)
		} else if f.Type == "file" {
			if !isFileToBeSkipped(f.Path) {
				totalSize += f.Size
				sizePerFile = append(sizePerFile, interfaces.RepoFileSize{Path: f.Path, Size: f.Size})
			}
		}
	}
	return totalSize, sizePerFile
}
