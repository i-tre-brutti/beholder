package plugins

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"sync"

	"github.com/peterhellberg/link"
	"gitlab.com/i-tre-brutti/common/utils"
)

func makeGithubAPICall(baseURL string, authToken utils.AuthHeader) ([]byte, error) {
	url := utils.AddParamsToURL(baseURL, []string{"page=1"})
	f, numPages := makeGithubSinglePageAPICall(url, authToken)
	var d []interface{}
	d = append(d, f...)
	if numPages > 1 {
		var waitGroup sync.WaitGroup
		for i := 2; i <= numPages; i++ {
			waitGroup.Add(1)
			go func(i int) {
				defer waitGroup.Done()
				url := utils.AddParamsToURL(baseURL, []string{fmt.Sprintf("page=%d", i)})
				f, _ := makeGithubSinglePageAPICall(url, authToken)
				d = append(d, f...)
			}(i)
		}
		waitGroup.Wait()
	}
	return json.Marshal(d)
}

func makeGithubSinglePageAPICall(endpoint string, authToken utils.AuthHeader) ([]interface{}, int) {
	var f []interface{}
	resp := utils.MakeAPICall(endpoint, authToken)
	err := json.NewDecoder(resp.Body).Decode(&f)
	defer resp.Body.Close()
	utils.E(err, 0)

	totalPages := 1
	for _, l := range link.ParseResponse(resp) {
		if l.Rel == "last" {
			u, err := url.Parse(l.URI)
			utils.E(err, 0)
			q := u.Query()
			totalPages, err = strconv.Atoi(q["page"][0])
			utils.E(err, 0)
		}
	}
	return f, totalPages
}
