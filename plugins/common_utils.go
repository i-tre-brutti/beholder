package plugins

import "gitlab.com/i-tre-brutti/common/utils"

func isFileToBeSkipped(filePath string) bool {
	return utils.IsImageFile(filePath) || utils.IsFontFile(filePath) || utils.IsAudioFile(filePath) || utils.IsVideoFile(filePath)
}
