package plugins

import "gitlab.com/i-tre-brutti/common/interfaces"

type IssuePlugin interface {
	Init(conf interfaces.IssueRepoConf, status interfaces.IssueData)
	Fetch() (interfaces.IssueData, int)
}

type SrcPlugin interface {
	Init(conf interfaces.SrcRepoConf, status interfaces.SrcData)
	Fetch() (interfaces.SrcData, string)
}
