package plugins

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

// The format is ISO8601 but I got weird errors trying to parse the true format.
// RFC3339 seems to be quite identical and it works
// https://stackoverflow.com/questions/38596079/how-do-i-parse-an-iso-8601-timestamp-in-golang
const gitlabDateFormat = time.RFC3339

// GetFileSize is the type for function that get the file size of a file from the API
type GetFileSize func(project, filePath string) interfaces.RepoFileSize

/*
 * Calculate the size of each file and the total size
 * Loops over all the files (excluding vendor) and calls getFileSize() to retrieve the size
 * SERIALIZED VERSION
 */
func serialCodeSizeFetcher(getFileSizeFn GetFileSize, project string, files []repoFile) ([]interfaces.RepoFileSize, int) {
	var sizePerFile []interfaces.RepoFileSize
	totalSize := 0

	for _, file := range files {
		// Only check files and skip vendor
		if file.Type == "blob" && !isFileToBeSkipped(file.Path) {
			result := getFileSizeFn(project, file.Path)
			totalSize = totalSize + result.Size
			sizePerFile = append(sizePerFile, interfaces.RepoFileSize{Path: result.Path, Size: result.Size})
		}
	}

	return sizePerFile, totalSize
}

/*
 * Calculate the size of each file and the total size
 * Loops over all the files (excluding vendor) and calls getFileSize() to retrieve the size
 * PARALLEL VERSION (too fast for GitLab, it kicks us out...)
 */
func parallelCodeSizeFetcher(getFileSizeFn GetFileSize, project string, files []repoFile) ([]interfaces.RepoFileSize, int) {
	var sizePerFile []interfaces.RepoFileSize
	ch := make(chan interfaces.RepoFileSize)

	var waitGroup sync.WaitGroup
	waitGroup.Add(len(files))
	for _, file := range files {
		go func(p, filePath, fileType string) {
			defer waitGroup.Done()
			if fileType == "blob" && !strings.HasPrefix(filePath, "vendor/") {
				ch <- getFileSizeFn(p, filePath)
			}
		}(project, file.Path, file.Type)
	}

	totalSize := 0
	sig := make(chan int)
	go func() {
		for result := range ch {
			totalSize = totalSize + result.Size
			sizePerFile = append(sizePerFile, interfaces.RepoFileSize{Path: result.Path, Size: result.Size})
		}
		sig <- 1
	}()
	waitGroup.Wait()
	close(ch)

	<-sig
	return sizePerFile, totalSize
}

/*
 * Returns a map with YYYYMM as key and as value the number of commits in that month
 */
func calculateCommitsPerMonth(commits []commit) map[string]int {
	commitsPerMonth := make(map[string]int)
	for _, c := range commits {
		t, err := time.Parse(gitlabDateFormat, c.CreatedAt)
		if err != nil {
			log.Printf("ERROR %vi\n", err)
			continue
		}
		dateStamp := fmt.Sprintf("%04d%02d", t.Year(), t.Month())
		commitsPerMonth[dateStamp]++
	}

	return commitsPerMonth
}

/*
 * Make calls to Gitlab APIs, managing paginated responses.
 */
func makeAPICall(baseURL string, authToken utils.AuthHeader) ([]byte, error) {
	baseURL = utils.AddParamsToURL(baseURL, []string{"per_page=50"})
	url := utils.AddParamsToURL(baseURL, []string{"page=1"})
	f, numPages := makeSinglePageAPICall(url, authToken)
	var d []interface{}
	d = append(d, f...)
	if numPages > 1 {
		var waitGroup sync.WaitGroup
		waitGroup.Add(numPages - 1)
		for i := 2; i <= numPages; i++ {
			go func(i int) {
				defer waitGroup.Done()
				url := utils.AddParamsToURL(baseURL, []string{fmt.Sprintf("page=%d", i)})
				f, _ := makeSinglePageAPICall(url, authToken)
				d = append(d, f...)
			}(i)
		}
		waitGroup.Wait()
	}
	return json.Marshal(d)
}

/*
 * Used by makeAPICall, calls a single page and returns its content
 */
func makeSinglePageAPICall(url string, authToken utils.AuthHeader) ([]interface{}, int) {
	var f []interface{}
	resp := utils.MakeAPICall(url, authToken)
	err := json.NewDecoder(resp.Body).Decode(&f)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("%s: reading response\n%s", url, err)
		panic(err)
	}
	i, err := strconv.Atoi(resp.Header["X-Total-Pages"][0])
	if err != nil {
		i = 1
		log.Println("Cannot convert", resp.Header["X-Total-Pages"])
	}
	return f, i
}
