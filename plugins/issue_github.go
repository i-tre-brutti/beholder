package plugins

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

// Init initialize the plugin setting the configuration and the previous status
func (v *IssueGithubPlugin) Init(conf interfaces.IssueRepoConf, status interfaces.IssueData) {
	v.conf = conf
	v.status = status
	v.baseURL = "https://api.github.com"
	v.authHeader = utils.AuthHeader{Name: "Authorization", Value: fmt.Sprintf("token %v", conf.AuthToken)}
}

// Fetch gets info from APIs and populates the new status
func (v *IssueGithubPlugin) Fetch() (interfaces.IssueData, int) {
	var lastIssueID int
	var newStatus interfaces.IssueData

	var waitGroup sync.WaitGroup
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus, lastIssueID = v.fetchIssues(v.conf.ProjectID)
	}()

	waitGroup.Wait()
	return newStatus, lastIssueID
}

func (v *IssueGithubPlugin) fetchIssues(project string) (interfaces.IssueData, int) {
	log.Println("Fetching issues for the project", project)
	var result interfaces.IssueData
	var lastIssueID int
	var issues []githubIssue

	res, err := makeGithubAPICall(fmt.Sprintf("%s/repos/%s/issues?state=all", v.baseURL, project), v.authHeader)
	utils.E(err, 1)

	err = json.Unmarshal(res, &issues)
	utils.E(err, 1)

	var openIssuesLifetime []int64
	var timeResolutions []int64

	for _, issue := range issues {
		if issue.Number > lastIssueID {
			lastIssueID = issue.Number
		}
		log.Printf("Analyzing issue #%d", issue.Number)
		result.Total++
		switch issue.State {
		case "open":
			result.Open++
			openIssuesLifetime = append(openIssuesLifetime, int64(time.Since(issue.CreatedAt)))
		case "closed":
			result.Resolved++
			timeResolutions = append(timeResolutions, int64(issue.ClosedAt.Sub(issue.CreatedAt)))
		}
	}
	result.AvgOpenIssuesLifetime = avg(openIssuesLifetime)
	result.AvgTimeResolution = avg(timeResolutions)

	return result, lastIssueID
}
