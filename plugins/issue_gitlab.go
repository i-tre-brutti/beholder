package plugins

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"sync"
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

// Init initialize the plugin setting the configuration and the previous status
func (v *IssueGitlabPlugin) Init(conf interfaces.IssueRepoConf, status interfaces.IssueData) {
	v.conf = conf
	v.status = status
	v.baseURL = conf.BaseURL
	if v.conf.BaseURL == "" {
		v.baseURL = "https://gitlab.com"
	}
	v.authHeader = utils.AuthHeader{Name: "PRIVATE-TOKEN", Value: conf.AuthToken}
}

// Fetch gets info from APIs and populates the new status
func (v *IssueGitlabPlugin) Fetch() (interfaces.IssueData, int) {
	var lastIssueID int
	project := url.PathEscape(v.conf.ProjectID)
	var newStatus interfaces.IssueData

	var waitGroup sync.WaitGroup
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		newStatus, lastIssueID = v.fetchIssues(project)
	}()

	waitGroup.Wait()
	return newStatus, lastIssueID
}

func (v *IssueGitlabPlugin) fetchIssues(project string) (interfaces.IssueData, int) {
	log.Println("Fetching issues for the project", project)
	var result interfaces.IssueData
	var lastIssueID int
	var issues []issue

	res, err := makeAPICall(fmt.Sprintf("%s/api/v4/projects/%s/issues?scope=all", v.baseURL, project), v.authHeader)
	utils.E(err, 1)

	err = json.Unmarshal(res, &issues)
	utils.E(err, 1)

	var openIssuesLifetime []int64
	var timeResolutions []int64

	for _, issue := range issues {
		if issue.ID > lastIssueID {
			lastIssueID = issue.ID
		}
		log.Printf("Analyzing issue #%d", issue.ID)
		result.Total++
		createdAt, _ := time.Parse(gitlabDateFormat, issue.CreatedAt)
		closedAt, _ := time.Parse(gitlabDateFormat, issue.ClosedAt)
		switch issue.State {
		case "opened":
			result.Open++
			openIssuesLifetime = append(openIssuesLifetime, int64(time.Since(createdAt)))
		case "closed":
			result.Resolved++
			timeResolutions = append(timeResolutions, int64(closedAt.Sub(createdAt)))
		}
	}
	result.AvgOpenIssuesLifetime = avg(openIssuesLifetime)
	result.AvgTimeResolution = avg(timeResolutions)

	return result, lastIssueID
}

func avg(values []int64) int64 {
	if len(values) == 0 {
		return 0
	}

	var total int64

	for _, v := range values {
		total += v
	}

	return total / int64(len(values))
}
