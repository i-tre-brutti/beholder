package plugins

import (
	"fmt"

	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

type SrcGitlabPlugin struct {
	conf       interfaces.SrcRepoConf
	status     interfaces.SrcData
	baseURL    string
	authHeader utils.AuthHeader
}

type IssueGitlabPlugin struct {
	conf       interfaces.IssueRepoConf
	status     interfaces.IssueData
	baseURL    string
	authHeader utils.AuthHeader
}

type issue struct {
	ID        int    `json:"iid"`
	State     string `json:"state"`
	CreatedAt string `json:"created_at"`
	ClosedAt  string `json:"closed_at"`
	// Other available fields
	//
	// UpdatedAt        string   `json:"updated_at"`
	// ProjectID        int      `json:"project_id"`
	// Title            int      `json:"title"`
	// Description      int      `json:"description"`
	// Labels           []string `json:"labels"`
	// Milestone        string   `json:"milestone"`
	// Assignees        []string `json:"assignees"`
	// Author           string   `json:"author"`
	// Assignee         int      `json:"assignee"`
	// UserNotesCount   int      `json:"user_notes_count"`
	// Upvotes          int      `json:"upvotes"`
	// Downvotes        int      `json:"downvotes"`
	// DueDate          int      `json:"due_date"`
	// Confidential     bool     `json:"confidential"`
	// Weight           int      `json:"weight"`
	// DiscussionLocked bool     `json:"discussion_locked"`
	// WebURL           bool     `json:"web_url"`
	// TimeStats        bool     `json:"time_stats"`
}

type commit struct {
	ID             string   `json:"id"`
	ShortID        string   `json:"short_id"`
	Title          string   `json:"title"`
	CreatedAt      string   `json:"created_at"`
	ParentIds      []string `json:"parent_ids"`
	Message        string   `json:"message"`
	AuthorName     string   `json:"author_name"`
	AuthorEmail    string   `json:"author_email"`
	AuthoredDate   string   `json:"authored_date"`
	CommitterName  string   `json:"committer_name"`
	CommitterEmail string   `json:"committer_email"`
	CommittedDate  string   `json:"committed_date"`
}

func (c *commit) String() string {
	return fmt.Sprintf("[%s:%s] <%s @%s> %s", c.ShortID, c.CreatedAt, c.AuthorName, c.AuthorEmail, c.Title)
}

type branch struct {
	Name   string `json:"name"`
	Commit commit `json:"commit"`
	Merged bool   `json:"merged"`
}

type mergeRequest struct {
	ID        int    `json:"iid"`
	Title     string `json:"title"`
	State     string `json:"state"`
	UpVotes   int    `json:"upvotes"`
	DownVotes int    `json:"downvotes"`
	Wip       bool   `json:"work_in_progress"`
}

type repoFile struct {
	ID   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
	Path string `json:"path"`
	Mode string `json:"mode"`
}

type repoFileDetails struct {
	FileName     string `json:"file_name"`
	FilePath     string `json:"file_path"`
	Size         int    `json:"size"`
	Encoding     string `json:"encoding"`
	Content      string `json:"content"`
	Ref          string `json:"ref"`
	BlobID       string `json:"blob_id"`
	CommitID     string `json:"commit_id"`
	LastCommitID string `json:"last_commit_id"`
}

type projectInfo struct {
	StarCount  int `json:"star_count"`
	ForksCount int `json:"forks_count"`
}

type contributor struct {
	Name    string `json:"name"`
	Email   string `json:"email"`
	Commits int    `json:"commits"`
}
