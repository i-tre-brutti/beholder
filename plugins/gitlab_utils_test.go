package plugins

import (
	"math/rand"
	"testing"
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
)

var fileSizes map[string]int

func mockGetFileSize(project, filePath string) interfaces.RepoFileSize {
	rand.Seed(time.Now().UTC().UnixNano())
	return interfaces.RepoFileSize{Path: filePath, Size: fileSizes[filePath]}
}

func TestSerialCodeSizeFetcher(t *testing.T) {
	fileSizes = make(map[string]int)

	project := "testProject"
	var files []repoFile

	total := 0

	f := repoFile{Type: "blob", Path: "b1"}
	fileSizes[f.Path] = rand.Intn(5000)
	total += fileSizes[f.Path]
	files = append(files, f)

	f = repoFile{Type: "blob", Path: "b2"}
	fileSizes[f.Path] = rand.Intn(5000)
	total += fileSizes[f.Path]
	files = append(files, f)

	// This isn't a blob, it mustn't be included in sum
	f = repoFile{Type: "tree", Path: "t1"}
	fileSizes[f.Path] = rand.Intn(5000)
	files = append(files, f)

	_, totalSize := serialCodeSizeFetcher(mockGetFileSize, project, files)

	if total != totalSize {
		t.Errorf("Wrong total size")
	}
}
