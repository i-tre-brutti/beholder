module gitlab.com/i-tre-brutti/beholder

go 1.14

require (
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/peterhellberg/link v1.0.0
	gitlab.com/i-tre-brutti/common v0.0.0-20180208220937-970a7d678b8e
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/mgo.v2 v2.0.0-20160818020120-3f83fa500528
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
