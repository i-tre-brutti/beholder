package analyzers

import (
	"fmt"
	"testing"

	"gitlab.com/i-tre-brutti/beholder/fixtures"
	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/test_utils"
)

func setUp(t *testing.T) (*projectAnalyzer, interfaces.ProjectStatus) {
	conf, status := _getStatus(t)

	analysis := new(projectAnalyzer)
	analysis.conf = conf

	return analysis, status
}

func _getStatus(t *testing.T) (interfaces.ProjectConf, interfaces.ProjectStatus) {
	t.Log("Should load the current status from the db", test_utils.CheckMark)
	collection := db.GetStatusCollection()

	conf := fixtures.GetProjectConf()
	status := fixtures.GetEmptyProjectStatus(conf.ProjectID)
	status.DocumentVersion = interfaces.ProjectStatusVersion

	collection.Insert(status)

	return conf, status
}

// func Test_projectAnalyzer(t *testing.T) {
// 	t.Log("Should be able to run the analysis of a conf")
// 	conf, status := _getStatus()

// 	var analysis IProjectAnalyzer
// 	analysis = new(projectAnalyzer)
// 	result := analysis.Run(conf)

// 	if !_areStatusesEqual(status, result) {
// 		t.Errorf("Should have returned a status like %v instead of %v. %v", status, result, ballotX)
// 	}
// 	t.Logf("Should return a valid final status. %v", checkMark)
// }

func TestSetLastSavedStatus(t *testing.T) {
	analysis, status := setUp(t)
	err := analysis.SetLastSavedStatus()
	test_utils.CheckTest(
		t,
		err == nil,
		"Should load the current status from the db",
		fmt.Sprintf("%v", err),
	)

	s := analysis.currentStatus

	test_utils.CheckTest(
		t,
		s.DocumentVersion == status.DocumentVersion,
		"Should load the correct document version",
		"",
	)

	t1 := s.LastRunInfo.Timestamp.Format("2006-01-02 15:04:05")
	t2 := status.LastRunInfo.Timestamp.Format("2006-01-02 15:04:05")
	test_utils.CheckTest(
		t,
		t1 == t2,
		"Should load the correct LastRunInfo",
		fmt.Sprintf("%v != %v", s.LastRunInfo.Timestamp, status.LastRunInfo.Timestamp),
	)
	test_utils.CheckTest(
		t,
		s.ProjectID == status.ProjectID,
		"Should load the correct ProjectID.",
		fmt.Sprintf("%v != %v", s.ProjectID, status.ProjectID),
	)
}

func TestMergeSrcData(t *testing.T) {
	analysis, _ := setUp(t)
	var sources []interfaces.SrcData

	s1 := fixtures.GetSrcData()
	s2 := fixtures.GetSrcData()
	s3 := fixtures.GetSrcData()

	sources = append(sources, s1)
	sources = append(sources, s2)
	sources = append(sources, s3)

	ret := analysis.mergeSrcData(sources)

	// PR
	prTestFlag := s1.Pr.Approved + s2.Pr.Approved + s3.Pr.Approved
	test_utils.CheckTest(
		t,
		ret.Pr.Approved == prTestFlag,
		"Should have the correct number of approved PRs",
		fmt.Sprintf("%v != %v", ret.Pr.Approved, prTestFlag),
	)

	prTestFlag = s1.Pr.Closed + s2.Pr.Closed + s3.Pr.Closed
	test_utils.CheckTest(
		t,
		ret.Pr.Closed == prTestFlag,
		"Should have the correct number of closed PRs",
		fmt.Sprintf("%v != %v", ret.Pr.Closed, prTestFlag),
	)

	prTestFlag = s1.Pr.Merged + s2.Pr.Merged + s3.Pr.Merged
	test_utils.CheckTest(
		t,
		ret.Pr.Merged == prTestFlag,
		"Should have the correct number of merged PRs",
		fmt.Sprintf("%v != %v", ret.Pr.Merged, prTestFlag),
	)

	prTestFlag = s1.Pr.Opened + s2.Pr.Opened + s3.Pr.Opened
	test_utils.CheckTest(
		t,
		ret.Pr.Opened == prTestFlag,
		"Should have the correct number of Opened PRs",
		fmt.Sprintf("%v != %v", ret.Pr.Opened, prTestFlag),
	)

	prTestFlag = s1.Pr.Rejected + s2.Pr.Rejected + s3.Pr.Rejected
	test_utils.CheckTest(
		t,
		ret.Pr.Rejected == prTestFlag,
		"Should have the correct number of Rejected PRs",
		fmt.Sprintf("%v != %v", ret.Pr.Rejected, prTestFlag),
	)

	prTestFlag = s1.Pr.Total + s2.Pr.Total + s3.Pr.Total
	test_utils.CheckTest(
		t,
		ret.Pr.Total == prTestFlag,
		"Should have the correct number of Total PRs",
		fmt.Sprintf("%v != %v", ret.Pr.Total, prTestFlag),
	)

	// Branches
	branchesTestFlag := s1.Branches.Merged + s2.Branches.Merged + s3.Branches.Merged
	test_utils.CheckTest(
		t,
		ret.Branches.Merged == branchesTestFlag,
		"Should have the correct number of Merged branches",
		fmt.Sprintf("%v != %v", ret.Branches.Merged, branchesTestFlag),
	)

	branchesTestFlag = s1.Branches.OpenAbandoned + s2.Branches.OpenAbandoned + s3.Branches.OpenAbandoned
	test_utils.CheckTest(
		t,
		ret.Branches.OpenAbandoned == branchesTestFlag,
		"Should have the correct number of OpenAbandoned branches",
		fmt.Sprintf("%v != %v", ret.Branches.OpenAbandoned, branchesTestFlag),
	)

	branchesTestFlag = s1.Branches.OpenWip + s2.Branches.OpenWip + s3.Branches.OpenWip
	test_utils.CheckTest(
		t,
		ret.Branches.OpenWip == branchesTestFlag,
		"Should have the correct number of OpenWip branches",
		fmt.Sprintf("%v != %v", ret.Branches.OpenWip, branchesTestFlag),
	)

	// Social
	socialTestFlag := s1.Social.Contributors + s2.Social.Contributors + s3.Social.Contributors
	test_utils.CheckTest(
		t,
		ret.Social.Contributors == socialTestFlag,
		"Should have the correct number of contributors",
		fmt.Sprintf("%v != %v", ret.Social.Contributors, socialTestFlag),
	)

	socialTestFlag = s1.Social.Forks + s2.Social.Forks + s3.Social.Forks
	test_utils.CheckTest(
		t,
		ret.Social.Forks == socialTestFlag,
		"Should have the correct number of Forks",
		fmt.Sprintf("%v != %v", ret.Social.Forks, socialTestFlag),
	)

	socialTestFlag = s1.Social.Stars + s2.Social.Stars + s3.Social.Stars
	test_utils.CheckTest(
		t,
		ret.Social.Stars == socialTestFlag,
		"Should have the correct number of Stars",
		fmt.Sprintf("%v != %v", ret.Social.Stars, socialTestFlag),
	)

	socialTestFlag = s1.Social.Watchers + s2.Social.Watchers + s3.Social.Watchers
	test_utils.CheckTest(
		t,
		ret.Social.Watchers == socialTestFlag,
		"Should have the correct number of Watchers",
		fmt.Sprintf("%v != %v", ret.Social.Watchers, socialTestFlag),
	)
}

func TestMergeIssueData(t *testing.T) {
	analysis, _ := setUp(t)

	i1 := fixtures.GetIssueData()
	i2 := fixtures.GetIssueData()
	i3 := fixtures.GetIssueData()

	ret := analysis.mergeIssueData([]interfaces.IssueData{i1, i2, i3})

	Open := i1.Open + i2.Open + i3.Open
	test_utils.CheckTest(
		t,
		ret.Open == Open,
		"Should have the correct number of open issues",
		fmt.Sprintf("%v != %v", ret.Open, Open),
	)

	Resolved := i1.Resolved + i2.Resolved + i3.Resolved
	test_utils.CheckTest(
		t,
		ret.Resolved == Resolved,
		"Should have the correct number of Resolved issues",
		fmt.Sprintf("%v != %v", ret.Resolved, Resolved),
	)

	Total := i1.Total + i2.Total + i3.Total
	test_utils.CheckTest(
		t,
		ret.Total == Total,
		"Should have the correct number of Total issues",
		fmt.Sprintf("%v != %v", ret.Total, Total),
	)

	AvgOpenIssuesLifetime := (i1.AvgOpenIssuesLifetime + i2.AvgOpenIssuesLifetime + i3.AvgOpenIssuesLifetime) / int64(3)
	test_utils.CheckTest(
		t,
		ret.AvgOpenIssuesLifetime == AvgOpenIssuesLifetime,
		"Should have the correct number of AvgOpenIssuesLifetime issues",
		fmt.Sprintf("%v != %v", ret.AvgOpenIssuesLifetime, AvgOpenIssuesLifetime),
	)

	AvgTimeResolution := (i1.AvgTimeResolution + i2.AvgTimeResolution + i3.AvgTimeResolution) / int64(3)
	test_utils.CheckTest(
		t,
		ret.AvgTimeResolution == AvgTimeResolution,
		"Should have the correct number of AvgTimeResolution issues",
		fmt.Sprintf("%v != %v", ret.AvgTimeResolution, AvgTimeResolution),
	)
}

// func TestAnalyzeIssues(t *testing.T) {
// 	var status interfaces.ProjectStatus
// 	conf := fixtures.GetProjectConf()

// 	_, lastIssuesDesc := analyzeIssues(mockIssueAnalyzer, conf, status)

// 	issueRes := 0
// 	for _, d := range lastIssuesDesc {
// 		issueRes += d.IssueID
// 	}
// 	if issueRes != 10*len(conf.Issues) {
// 		t.Errorf("Wrong lastIssueDesc sum: %v", issueRes)
// 	}
// }

// Need to mock http calls here...
// func TestAnalyzeSources(t *testing.T) {
// 	analysis, _ := setUp(t)

// 	analysis.analyzeSources()
// 	test_utils.CheckTest(
// 		t,
// 		analysis.finalStatus.LastRunInfo.Commits[0].CommitID == "commitID",
// 		"Should have the correct Commit ID",
// 		"",
// 	)
// }

func _areStatusesEqual(s1, s2 interfaces.ProjectStatus) bool {
	if s1.DocumentVersion != s2.DocumentVersion {
		return false
	}

	if s1.ProjectID != s2.ProjectID {
		return false
	}

	// TODO: Add further comparisons

	return true
}
