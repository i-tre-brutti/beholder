package analyzers

import (
	"log"
	"sync"

	"gitlab.com/i-tre-brutti/beholder/plugins"
	"gitlab.com/i-tre-brutti/common/interfaces"
)

func (a *projectAnalyzer) analyzeSources() {
	lastCommits := make([]interfaces.LastCommitsDesc, len(a.conf.Sources))
	sources := make([]interfaces.SrcData, len(a.conf.Sources))

	var waitGroup sync.WaitGroup
	for i, src := range a.conf.Sources {
		waitGroup.Add(1)
		go func(src interfaces.SrcConf, idx int) {
			defer waitGroup.Done()
			res, commitID := a.sourceAnalyzer(src)
			lastCommits[idx] = interfaces.LastCommitsDesc{ProjectID: src.Conf.ProjectID, CommitID: commitID}
			sources[idx] = res
		}(src, i)
	}
	waitGroup.Wait()
	a.finalStatus.LastRunInfo.Commits = lastCommits
	a.finalStatus.Src = a.mergeSrcData(sources)
}

func (a *projectAnalyzer) mergeSrcData(sources []interfaces.SrcData) interfaces.SrcData {
	var result interfaces.SrcData
	result.Code.CommitsPerMonth = make(map[string]int)
	for _, src := range sources {
		// Code
		if result.Code.FirstCommit.IsZero() || src.Code.FirstCommit.Before(result.Code.FirstCommit) {
			result.Code.FirstCommit = src.Code.FirstCommit
		}

		if result.Code.LastCommit.IsZero() || src.Code.LastCommit.After(result.Code.LastCommit) {
			result.Code.LastCommit = src.Code.LastCommit
		}
		result.Code.TotalCodeSize += src.Code.TotalCodeSize
		result.Code.CodeSizePerFile = append(result.Code.CodeSizePerFile, src.Code.CodeSizePerFile...)

		for date, value := range src.Code.CommitsPerMonth {
			result.Code.CommitsPerMonth[date] += value
		}
		// Branches
		result.Branches.Merged += src.Branches.Merged
		result.Branches.OpenAbandoned += src.Branches.OpenAbandoned
		result.Branches.OpenWip += src.Branches.OpenWip
		// Social
		result.Social.Contributors += src.Social.Contributors
		result.Social.Forks += src.Social.Forks
		result.Social.Stars += src.Social.Stars
		result.Social.Watchers += src.Social.Watchers
		// PR
		result.Pr.Approved += src.Pr.Approved
		result.Pr.Closed += src.Pr.Closed
		result.Pr.Merged += src.Pr.Merged
		result.Pr.Opened += src.Pr.Opened
		result.Pr.Rejected += src.Pr.Rejected
		result.Pr.Total += src.Pr.Total
	}
	return result
}

func (a *projectAnalyzer) sourceAnalyzer(src interfaces.SrcConf) (interfaces.SrcData, string) {
	var plugin plugins.SrcPlugin
	var newStatus interfaces.SrcData
	var lastCommitID string
	switch src.RepoType {
	case "gitlab":
		plugin = new(plugins.SrcGitlabPlugin)
		plugin.Init(src.Conf, a.currentStatus.Src)
		newStatus, lastCommitID = plugin.Fetch()
	case "github":
		plugin = new(plugins.SrcGithubPlugin)
		plugin.Init(src.Conf, a.currentStatus.Src)
		newStatus, lastCommitID = plugin.Fetch()
	default:
		log.Println("Plugin not available for source type", src.RepoType)
	}
	return newStatus, lastCommitID
}
