package analyzers

import (
	"sync"
	"time"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/i-tre-brutti/common/db"
	"gitlab.com/i-tre-brutti/common/interfaces"
	"gitlab.com/i-tre-brutti/common/utils"
)

/*
ProjectAnalyzer manages the analysis of the provided project, spawning two goroutines:
	- issues analysis
	- source code analysis

It returns an interfaces.ProjectStatus
*/

type IProjectAnalyzer interface {
	Run(conf interfaces.ProjectConf) interfaces.ProjectStatus
	SetLastSavedStatus() error
}

type projectAnalyzer struct {
	conf          interfaces.ProjectConf
	finalStatus   interfaces.ProjectStatus
	currentStatus interfaces.ProjectStatus
}

// StartAnalysis actually runs the analysis
func StartAnalysis(conf interfaces.ProjectConf) interfaces.ProjectStatus {
	analysis := new(projectAnalyzer)
	return analysis.Run(conf)
}

func (a *projectAnalyzer) Run(conf interfaces.ProjectConf) interfaces.ProjectStatus {
	a.conf = conf

	// Last saved status of the project
	err := a.SetLastSavedStatus()
	utils.E(err, 0)

	var waitGroup sync.WaitGroup

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		a.analyzeSources()
	}()

	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		a.analyzeIssues()
	}()

	waitGroup.Wait()

	a.finalStatus.DocumentVersion = interfaces.ProjectStatusVersion
	a.finalStatus.ProjectID = conf.ProjectID
	// Formats at https://golang.org/pkg/time/#pkg-constants
	a.finalStatus.LastRunInfo.Timestamp = time.Now()
	return a.finalStatus
}

// SetLastSavedStatus loads the last saved status of the project from the db
func (a *projectAnalyzer) SetLastSavedStatus() error {
	collection := db.GetStatusCollection()
	err := collection.Find(
		bson.M{"project_id": a.conf.ProjectID, "document_version": interfaces.ProjectStatusVersion},
	).One(&a.currentStatus)

	if err != nil && err.Error() != "not found" {
		return err
	}

	return nil
}
