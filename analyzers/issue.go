package analyzers

import (
	"log"
	"sync"

	"gitlab.com/i-tre-brutti/beholder/plugins"
	"gitlab.com/i-tre-brutti/common/interfaces"
)

func (a *projectAnalyzer) analyzeIssues() {

	lastIssues := make([]interfaces.LastIssuesDesc, len(a.conf.Issues))
	issues := make([]interfaces.IssueData, len(a.conf.Issues))

	var waitGroup sync.WaitGroup

	for i, issue := range a.conf.Issues {
		waitGroup.Add(1)
		go func(issue interfaces.IssueConf, idx int) {
			defer waitGroup.Done()
			res, issueID := issueAnalyzer(issue, a.currentStatus.Issue)
			lastIssues[idx] = interfaces.LastIssuesDesc{ProjectID: issue.Conf.ProjectID, IssueID: issueID}
			issues[idx] = res
		}(issue, i)
	}

	waitGroup.Wait()

	a.finalStatus.Issue = a.mergeIssueData(issues)
	a.finalStatus.LastRunInfo.Issues = lastIssues
}

func (a *projectAnalyzer) mergeIssueData(issues []interfaces.IssueData) interfaces.IssueData {
	var result interfaces.IssueData
	for _, issue := range issues {
		result.Open += issue.Open
		result.Resolved += issue.Resolved
		result.Total += issue.Total
		result.AvgOpenIssuesLifetime += issue.AvgOpenIssuesLifetime
		result.AvgTimeResolution += issue.AvgTimeResolution
	}
	if len(issues) > 0 {
		result.AvgOpenIssuesLifetime = result.AvgOpenIssuesLifetime / int64(len(issues))
		result.AvgTimeResolution = result.AvgTimeResolution / int64(len(issues))
	}

	return result
}

func issueAnalyzer(issue interfaces.IssueConf, status interfaces.IssueData) (interfaces.IssueData, int) {
	var plugin plugins.IssuePlugin
	var newStatus interfaces.IssueData
	var lastIssueID int
	switch issue.RepoType {
	case "gitlab":
		plugin = new(plugins.IssueGitlabPlugin)
		plugin.Init(issue.Conf, status)
		newStatus, lastIssueID = plugin.Fetch()
	case "github":
		plugin = new(plugins.IssueGithubPlugin)
		plugin.Init(issue.Conf, status)
		newStatus, lastIssueID = plugin.Fetch()
	default:
		log.Println("Plugin not available for source type", issue.RepoType)
	}
	return newStatus, lastIssueID
}
