package fixtures

import (
	"math/rand"
	"strconv"
	"time"

	"gitlab.com/i-tre-brutti/common/interfaces"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func GetEmptyProjectStatus(projectID string) interfaces.ProjectStatus {
	status := interfaces.ProjectStatus{ProjectID: projectID}
	status.LastRunInfo.Timestamp = time.Now()

	return status
}

func UpdateProjectStatusTimestamp(status *interfaces.ProjectStatus) {
	status.LastRunInfo.Timestamp = time.Now()
}

// GetProjectConf returns a fake project configuration
func GetProjectConf() interfaces.ProjectConf {
	var conf interfaces.ProjectConf
	conf.ProjectID = "test_project_" + strconv.Itoa(rand.Intn(500))
	conf.Name = "Test Project"

	// SOURCE
	var sources []interfaces.SrcConf

	var s1 interfaces.SrcConf
	s1.RepoType = "gitlab"
	s1.Conf = interfaces.SrcRepoConf{ProjectID: "test_src_1"}
	sources = append(sources, s1)

	conf.Sources = sources
	// ISSUES
	var issues []interfaces.IssueConf

	var i1 interfaces.IssueConf
	i1.RepoType = "gitlab"
	i1.Conf = interfaces.IssueRepoConf{ProjectID: "test_issue_1"}
	issues = append(issues, i1)

	var i2 interfaces.IssueConf
	i2.RepoType = "gitlab"
	i2.Conf = interfaces.IssueRepoConf{ProjectID: "test_issue_2"}
	issues = append(issues, i2)

	conf.Issues = issues

	return conf
}

// GetSrcData generates random interfaces.SrcData
func GetSrcData() interfaces.SrcData {
	var ret interfaces.SrcData

	// Code
	f1 := interfaces.RepoFileSize{Path: "f1", Size: (rand.Intn(50) + 1) * 10000}
	f2 := interfaces.RepoFileSize{Path: "f2", Size: (rand.Intn(50) + 1) * 10000}
	f3 := interfaces.RepoFileSize{Path: "f3", Size: (rand.Intn(50) + 1) * 10000}
	f4 := interfaces.RepoFileSize{Path: "f4", Size: (rand.Intn(50) + 1) * 10000}
	ret.Code.CodeSizePerFile = []interfaces.RepoFileSize{f1, f2, f3, f4}
	ret.Code.CommitsPerMonth = map[string]int{
		"201710": rand.Intn(10),
		"201709": rand.Intn(30),
		"201708": rand.Intn(30),
		"201707": rand.Intn(30),
	}
	ret.Code.FirstCommit = time.Now().AddDate(-rand.Intn(5), rand.Intn(12), 0)
	ret.Code.LastCommit = time.Now().AddDate(0, 0, -rand.Intn(15))
	ret.Code.TotalCodeSize = (rand.Intn(50) + 1) * 10000

	// Pr
	ret.Pr.Approved = rand.Intn(40)
	ret.Pr.Closed = rand.Intn(15)
	ret.Pr.Merged = ret.Pr.Approved - rand.Intn(5)
	ret.Pr.Opened = rand.Intn(20)
	ret.Pr.Rejected = rand.Intn(10)
	ret.Pr.Total = ret.Pr.Approved + ret.Pr.Closed + ret.Pr.Merged + ret.Pr.Opened + ret.Pr.Rejected

	// Branches
	ret.Branches.Merged = rand.Intn(50)
	ret.Branches.OpenAbandoned = rand.Intn(10)
	ret.Branches.OpenWip = rand.Intn(10)

	// Social
	ret.Social.Contributors = rand.Intn(30)
	ret.Social.Forks = rand.Intn(80)
	ret.Social.Stars = rand.Intn(200)
	ret.Social.Watchers = rand.Intn(60)

	return ret
}

// GetIssueData generates random interfaces.IssueData
func GetIssueData() interfaces.IssueData {
	var ret interfaces.IssueData

	ret.Open = rand.Intn(80)
	ret.Resolved = rand.Intn(80)
	ret.Total = ret.Open + ret.Resolved
	ret.AvgOpenIssuesLifetime = rand.Int63()
	ret.AvgTimeResolution = rand.Int63()

	return ret
}
