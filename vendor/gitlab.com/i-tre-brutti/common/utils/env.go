package utils

import (
	"flag"
	"os"
)

// IsTestingEnv return whether the code is running in testing environment
func IsTestingEnv() bool {
	v := flag.Lookup("test.v")
	env := os.Getenv("ENV")
	return (v != nil && v.Value.String() == "true") || env == "test"
}

// IsDebugEnv identifies debugging env. Run with `DEBUG=1 ./command`
func IsDebugEnv() bool {
	return os.Getenv("DEBUG") == "1"
}
