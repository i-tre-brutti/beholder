package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"
)

const maxRetries = 3

// AuthHeader permits to build a header request to make authenticated calls
type AuthHeader struct {
	Name  string
	Value string
}

// GetJSON gets and decodes a generic JSON object from a URL, trying multiple times before surrender
func GetJSON(url string, authToken AuthHeader, obj interface{}) error {
	var result interface{}
	for i := 1; i <= maxRetries; i++ {
		resp := MakeAPICall(url, authToken)
		err := json.NewDecoder(resp.Body).Decode(&obj)
		defer resp.Body.Close()
		if err != nil {
			log.Printf("%s: reading response\n[ERR] %s", url, err)
			if i >= maxRetries {
				return err
			}
		} else {
			obj = result
			break
		}
	}
	return nil
}

func getSourceFromURL(url string) string {
	if strings.Contains(url, "gitlab.com") {
		return "gitlab"
	} else if strings.Contains(url, "github.com") {
		return "github"
	}

	panic(fmt.Sprintf("\nERROR: Not supported source type in URL: %s", url))
}

// MakeAPICall makes a call to the provided url and returns the response.
// It tries multiple times before panicking
func MakeAPICall(url string, authToken AuthHeader) *http.Response {
	client := &http.Client{}

	var resp *http.Response
	var errorsList []error
	for i := 1; i <= maxRetries; i++ {
		req, err := http.NewRequest("GET", url, nil)

		// Add auth header if provided
		if authToken.Value != "" {
			req.Header.Add(authToken.Name, authToken.Value)
		}

		r, err := client.Do(req)
		if err != nil {
			log.Printf("#%d %s: %s\n", i, url, err)
			errorsList = append(errorsList, err)
			if i >= maxRetries {
				for _, e := range errorsList {
					log.Println(e)
				}
				panic("Too many errors")
			}
		} else {
			if r.StatusCode >= 400 {
				errorsList = append(errorsList, errors.New(r.Status))
				if i >= maxRetries {
					for _, e := range errorsList {
						log.Println(e)
					}
					panic("Too many errors")
				}

				if r.StatusCode == 429 {
					log.Println("Got 429 too many requests, let's try to wait 10 seconds...")
					time.Sleep(10 * time.Second)
				}
				continue
			}
			resp = r
			break
		}
	}
	return resp
}

// AddParamsToURL receives a URL and a list of params to add to it, and returns a well formed URL
func AddParamsToURL(baseURL string, params []string) string {
	// remove the ? char if it's the last char of the url
	var re = regexp.MustCompile(`^(.*)\?$`)
	url := re.ReplaceAllString(baseURL, `${1}`)

	separator := GetURLParametersSeparator(url)
	mergedParams := strings.Join(params, "&")
	return fmt.Sprintf("%s%s%s", url, separator, mergedParams)
}

// GetURLParametersSeparator returns the char to use as separator to append new parameters to a URL.
// If the given URL already contains a ?, it should already contain parameters, so use & as
// separator. Otherwise, use ?
func GetURLParametersSeparator(url string) string {
	divChar := "?"
	if strings.Contains(url, "?") {
		divChar = "&"
	}
	return divChar
}
