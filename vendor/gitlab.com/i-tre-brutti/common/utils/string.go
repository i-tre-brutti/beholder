package utils

import (
	"regexp"
	"strings"
)

var camelingRegex = regexp.MustCompile("[0-9A-Za-z]+")

// SnakeCaseToCamelCase - converts snake case strings to their camel case equivalent
// https://github.com/etgryphon/stringUp
func SnakeCaseToCamelCase(inputUnderScoreStr string) string {
	var re = regexp.MustCompile(`(?i)^(.*)(_?)id$`)
	s := re.ReplaceAllString(inputUnderScoreStr, `${1}${2}ID`)

	isToUpper := false
	var camelCase string
	for k, v := range s {
		if k == 0 {
			camelCase = strings.ToUpper(string(s[0]))
		} else {
			if isToUpper {
				camelCase += strings.ToUpper(string(v))
				isToUpper = false
			} else {
				if v == '_' {
					isToUpper = true
				} else {
					camelCase += string(v)
				}
			}
		}
	}
	return camelCase

}
