package utils

import (
	"log"
)

/*
E is a simple function to manage errors instead of do it everywhere. Just call it with the
error and a level to do the job

Levels:
  0 - panic
  1 - log.Fatalln
  2 - log.Println
*/
func E(e error, level int) {
	if e != nil {
		switch level {
		case 1:
			log.Fatalln(e)
		case 2:
			log.Println(e)
		default:
			panic(e)
		}
	}
}
