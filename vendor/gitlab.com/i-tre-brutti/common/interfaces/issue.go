package interfaces

type IssueData struct {
	Open                  int   `json:"open"`
	Resolved              int   `json:"resolved"`
	Total                 int   `json:"total"`
	AvgOpenIssuesLifetime int64 `json:"avg_open_issues_lifetime"`
	AvgTimeResolution     int64 `json:"avg_time_resolution"`
}
