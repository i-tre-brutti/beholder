package interfaces

// SrcPlugin ok

// GlobalConf contains the main app configuration
type GlobalConf struct {
	Host                 string
	Db                   string
	ConfCollection       string
	LiveEventsCollection string
	StatusCollection     string
	StatisticsCollection string
}

// ProjectConf is the base struct containing configuration about the Project to analyze
type ProjectConf struct {
	ProjectID string      `json:"project_id" bson:"project_id"`
	Name      string      `json:"name" bson:"name"`
	Sources   []SrcConf   `json:"sources" bson:"sources"`
	Issues    []IssueConf `json:"issues" bson:"issues"`
}

// SrcConf is part of a Project configuration and identifies the type of repo and contains
// the repo configuration
type SrcConf struct {
	RepoType string      `json:"repoType" bson:"repoType"`
	Conf     SrcRepoConf `json:"conf" bson:"conf"`
}

// SrcRepoConf contains all the configuration of the repo, needed by the plugins to retrieve info
type SrcRepoConf struct {
	BaseURL   string `json:"baseURL" bson:"baseURL"`
	AuthToken string `json:"authToken" bson:"authToken"`
	ProjectID string `json:"projectID" bson:"projectID"`
}

// IssueConf contains the configuration of the issue tracker
type IssueConf struct {
	RepoType string        `json:"repoType" bson:"repoType"`
	Conf     IssueRepoConf `json:"conf" bson:"conf"`
}

// IssueRepoConf contains all the configuration of the repo, needed by the plugins to retrieve info
type IssueRepoConf struct {
	BaseURL   string `json:"baseURL" bson:"baseURL"`
	AuthToken string `json:"authToken" bson:"authToken"`
	ProjectID string `json:"projectID" bson:"projectID"`
}
